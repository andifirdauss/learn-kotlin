class _002_TypeData {
    fun Main(){
        val myInt: Int = 90
        val myDouble: Double = 3.14
        val myChar: Char = 'A'
        val myString: String = "Hello World"
        val myBoolean: Boolean = true

        println("myInt = $myInt")
        println("myInt = $myDouble")
        println("myInt = $myChar")
        println("myInt = $myString")
        println("myInt = $myBoolean")
    }
}