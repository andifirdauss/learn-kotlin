class _001_ValVar {
    fun Main(){
        //val or var is fluid variable where they can fit into what value we declare
        //but it's must declaration variable within value

        val score = 90
        //var working like const so it can be change after declare
        var phi = 3.14

        println(score)
        println(phi)
    }
}